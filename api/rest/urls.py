from django.urls import path, include
from rest_framework import routers
from rest.views import *

route = routers.DefaultRouter(trailing_slash=True)

route.register('equipes', EquipeViewSet)
route.register('academias', AcademiaViewSet)
route.register('professores', ProfessorViewSet)
route.register('inscricoes', InscricaoViewSet)
route.register('configuracoes', ConfiguracoesViewSet, base_name='configuracoes')

urlpatterns = route.urls
