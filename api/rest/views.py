from django.contrib.auth.models import AnonymousUser
from rest_framework import viewsets, response, permissions
from rest_framework.decorators import action

from rest.serializers import *


class EquipeViewSet(viewsets.ModelViewSet):
    serializer_class = EquipeSerializer
    queryset = Equipe.objects.all()
    permission_classes = (permissions.IsAdminUser,)


class AcademiaViewSet(viewsets.ModelViewSet):
    serializer_class = AcademiaSerializer
    queryset = Academia.objects.all()
    permission_classes = (permissions.IsAdminUser,)


class ProfessorViewSet(viewsets.ModelViewSet):
    serializer_class = ProfessorSerializer
    queryset = Professor.objects.all()
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)


    @action(methods=['get'], detail=False)
    def eu(self, request, pk=None):
        data = Professor.objects.filter(email=self.request.user.email).first()
        return response.Response(self.serializer_class(data).data, status=200)


class InscricaoViewSet(viewsets.ModelViewSet):
    serializer_class = InscricaoSerializer
    queryset = Inscricao.objects.all()
    filter_fields = ('professor',)
    permission_classes = (permissions.AllowAny,)

    def get_queryset(self):
        qs = self.queryset
        if self.request.user.is_superuser:
            return qs
        qs = qs.filter(professor__email=self.request.user.email)
        return qs


class ConfiguracoesViewSet(viewsets.ViewSet):
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
    def list(self, request):
        data = dict(faixas=[dict(id=i[0], nome=i[1]) for i in Inscricao.FAIXA],
                    pesos=[dict(id=i[0], nome=i[1]) for i in Inscricao.CATEGORIA_PESO],
                    idades=[dict(id=i[0], nome=i[1]) for i in Inscricao.CATEGORIA_IDADE],
                    sexos=[dict(id=i[0], nome=i[1]) for i in Inscricao.SEXO])

        return response.Response(data, status=200)
