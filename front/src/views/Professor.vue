<template>
  <div id="equipes">
    <form-professor ref="formProfessor"/>
    <b-table striped
             hover
             stacked="sm"
             :busy="false"
             :fields="fields"
             :items="professores">
      <template slot="actions" slot-scope="row">
        <v-btn small outline icon color="info" @click.stop="edit(row.item)">
          <v-icon small>edit</v-icon>
        </v-btn>
        <v-btn small outline icon color="error" @click.stop="remove(row.item)">
          <v-icon small>close</v-icon>
        </v-btn>
      </template>
    </b-table>
    <v-btn
      fab
      bottom
      right
      color="primary"
      dark
      fixed
      @click="showForm"
    >
      <v-icon>add</v-icon>
    </v-btn>
  </div>
</template>

<script>
import { mapActions, mapState } from 'vuex'
export default {
  name: 'Professor',
  data () {
    return {
      fields: [
        {key: 'id', label: '#', sortable: true},
        {key: 'nome', label: 'Nome', sortable: true},
        {key: 'email', label: 'Email', sortable: true},
        {key: 'telefone', label: 'Telefone', sortable: true},
        {key: 'academia_display', label: 'Academia', sortable: true},
        {key: 'actions', label: 'Opções'}
      ],
      config: {
        state: 'professores',
        filters: {},
        url: 'api/professores/'
      }
    }
  },
  computed: {
    ...mapState(['professores'])
  },
  methods: {
    ...mapActions(['load']),
    edit (item) {
      this.$refs.formProfessor.show(this.config, item)
    },
    remove (item) {
      this.$refs.formProfessor.delete(this.config, item)
    },
    showForm () {
      this.$refs.formProfessor.show(this.config)
    }
  },
  async created () {
    await this.load(this.config)
  }
}
</script>
