import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/views/Home'
import Equipe from '@/views/Equipe'
import Academia from '@/views/Academia'
import Professor from '@/views/Professor'
import Inscricao from '@/views/Inscricao'
import Confirmacao from '@/views/Confirmacao'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/inscricao',
      name: 'Inscricao',
      component: Inscricao
    },
    {
      path: '/confirmacao',
      name: 'Confirmacao',
      component: Confirmacao
    },
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/equipes',
      name: 'Equipes',
      component: Equipe
    },
    {
      path: '/academias',
      name: 'Academias',
      component: Academia
    },
    {
      path: '/professores',
      name: 'Professores',
      component: Professor
    }
  ]
})
