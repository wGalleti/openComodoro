# Open comodoro 2018

Sistema de inscrição para atletas no campeonato de 2018

## Dev

Instale o Python 3.6.5+

Instale o Node 8+

Clone o repositório: `git clone https://gitlab.com/wGalleti/openComodoro.git`

### Backend

Instale o `pipenv` de forma global no python `pip install pipenv`

Caso estiver no windows

```bash
cd api
python -v venv .venv
.venv\Scripts\activate.bat
pipenv install
pipenv install --dev
python manage.py migrate
python manage.py runserver
```

Caso estiver no linux ou osx

```bash
cd api
python -v venv .venv
source .venv\bin\activate
pipenv install
pipenv install --dev
python manage.py migrate
python manage.py runserver
```

### Frontend

```bash
cd front
npm install
npm run dev
```
